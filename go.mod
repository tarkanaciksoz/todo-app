module github.com/tarkanaciksoz/todo-list

go 1.19

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	gorm.io/driver/mysql v1.4.1
	gorm.io/gorm v1.24.0
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)
